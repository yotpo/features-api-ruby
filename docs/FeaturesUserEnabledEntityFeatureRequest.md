# FeaturesClient::FeaturesUserEnabledEntityFeatureRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entity** | [**FeaturesEntity**](FeaturesEntity.md) |  | [optional] 
**feature_name** | **String** |  | [optional] 
**user_enabled** | **BOOLEAN** |  | [optional] 


