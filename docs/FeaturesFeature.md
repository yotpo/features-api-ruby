# FeaturesClient::FeaturesFeature

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**version** | **String** |  | [optional] 
**dependees** | **Array&lt;String&gt;** |  | [optional] 
**dependers** | **Array&lt;String&gt;** |  | [optional] 
**settings** | [**Hash&lt;String, FeaturesFeatureSetting&gt;**](FeaturesFeatureSetting.md) |  | [optional] 
**owner_type** | **String** |  | [optional] 
**default_user_enabled** | **BOOLEAN** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 


