# FeaturesClient::FeaturesEntityFeatureSetting

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**int_value** | **String** |  | [optional] 
**nested_hash** | [**FeaturesNestedHash**](FeaturesNestedHash.md) |  | [optional] 
**simple_hash** | [**FeaturesSimpleHash**](FeaturesSimpleHash.md) |  | [optional] 
**string_array** | **String** |  | [optional] 
**string_value** | **String** |  | [optional] 


