# FeaturesClient::FeaturesSetStateResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**changed** | **BOOLEAN** |  | [optional] 


