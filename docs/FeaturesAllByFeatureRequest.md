# FeaturesClient::FeaturesAllByFeatureRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**feature_name** | **String** |  | [optional] 
**entity_type** | **String** |  | [optional] 


