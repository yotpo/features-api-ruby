# FeaturesClient::FeaturesFeatureList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**features** | **Array&lt;String&gt;** |  | [optional] 


