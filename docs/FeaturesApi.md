# FeaturesClient::FeaturesApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_feature**](FeaturesApi.md#add_feature) | **POST** /v1/features | 
[**get_all_features**](FeaturesApi.md#get_all_features) | **GET** /v1/features | 
[**get_feature**](FeaturesApi.md#get_feature) | **GET** /v1/features/{name} | 
[**get_feature_0**](FeaturesApi.md#get_feature_0) | **GET** /v1/features/{name}/{version} | 


# **add_feature**
> FeaturesActionResponse add_feature(body)



### Example
```ruby
# load the gem
require 'features-client'

api_instance = FeaturesClient::FeaturesApi.new

body = FeaturesClient::FeaturesFeature.new # FeaturesFeature | 


begin
  result = api_instance.add_feature(body)
  p result
rescue FeaturesClient::ApiError => e
  puts "Exception when calling FeaturesApi->add_feature: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**FeaturesFeature**](FeaturesFeature.md)|  | 

### Return type

[**FeaturesActionResponse**](FeaturesActionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **get_all_features**
> FeaturesFeatureList get_all_features



### Example
```ruby
# load the gem
require 'features-client'

api_instance = FeaturesClient::FeaturesApi.new

begin
  result = api_instance.get_all_features
  p result
rescue FeaturesClient::ApiError => e
  puts "Exception when calling FeaturesApi->get_all_features: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**FeaturesFeatureList**](FeaturesFeatureList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **get_feature**
> FeaturesFeatureResponse get_feature(name, opts)



### Example
```ruby
# load the gem
require 'features-client'

api_instance = FeaturesClient::FeaturesApi.new

name = "name_example" # String | 

opts = { 
  version: "version_example" # String | 
}

begin
  result = api_instance.get_feature(name, opts)
  p result
rescue FeaturesClient::ApiError => e
  puts "Exception when calling FeaturesApi->get_feature: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  | 
 **version** | **String**|  | [optional] 

### Return type

[**FeaturesFeatureResponse**](FeaturesFeatureResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **get_feature_0**
> FeaturesFeatureResponse get_feature_0(name, version)



### Example
```ruby
# load the gem
require 'features-client'

api_instance = FeaturesClient::FeaturesApi.new

name = "name_example" # String | 

version = "version_example" # String | 


begin
  result = api_instance.get_feature_0(name, version)
  p result
rescue FeaturesClient::ApiError => e
  puts "Exception when calling FeaturesApi->get_feature_0: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  | 
 **version** | **String**|  | 

### Return type

[**FeaturesFeatureResponse**](FeaturesFeatureResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



