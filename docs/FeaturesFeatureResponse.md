# FeaturesClient::FeaturesFeatureResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**feature** | [**FeaturesFeature**](FeaturesFeature.md) |  | [optional] 


