# FeaturesClient::FeaturesEntityFeatureSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**settings** | **Hash&lt;String, String&gt;** |  | [optional] 
**old_settings** | **Hash&lt;String, String&gt;** |  | [optional] 
**changed_keys** | **Array&lt;String&gt;** |  | [optional] 


