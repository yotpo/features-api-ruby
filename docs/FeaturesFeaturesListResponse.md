# FeaturesClient::FeaturesFeaturesListResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**features** | [**Hash&lt;String, FeaturesFeatureEnabledResponse&gt;**](FeaturesFeatureEnabledResponse.md) |  | [optional] 


