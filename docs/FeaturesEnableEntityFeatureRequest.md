# FeaturesClient::FeaturesEnableEntityFeatureRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entity** | [**FeaturesEntity**](FeaturesEntity.md) |  | [optional] 
**feature_name** | **String** |  | [optional] 
**skip_dependencies** | **BOOLEAN** |  | [optional] 
**user_enabled** | [**FeaturesNilOrBool**](FeaturesNilOrBool.md) |  | [optional] 


