# FeaturesClient::FeaturesNestedHash

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nested_hash** | [**Hash&lt;String, FeaturesSimpleHash&gt;**](FeaturesSimpleHash.md) |  | [optional] 


