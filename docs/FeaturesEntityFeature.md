# FeaturesClient::FeaturesEntityFeature

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entity** | [**FeaturesEntity**](FeaturesEntity.md) |  | [optional] 
**feature_name** | **String** |  | [optional] 
**feature_version** | **String** |  | [optional] 
**user_enabled** | **BOOLEAN** |  | [optional] 
**disabled** | **BOOLEAN** |  | [optional] 
**settings** | **Hash&lt;String, String&gt;** |  | [optional] 
**user_settings** | **Hash&lt;String, BOOLEAN&gt;** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 
**feature_state** | **String** |  | [optional] [default to &quot;DEFAULT&quot;]
**manual** | **BOOLEAN** |  | [optional] 


