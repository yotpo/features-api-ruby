# FeaturesClient::FeaturesFeatureEnabledResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **BOOLEAN** |  | [optional] 
**user_enabled** | **BOOLEAN** |  | [optional] 
**disabled** | **BOOLEAN** |  | [optional] 


