# FeaturesClient::FeaturesEntityFeaturesList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entity_features** | [**Array&lt;FeaturesEntityFeature&gt;**](FeaturesEntityFeature.md) |  | [optional] 


