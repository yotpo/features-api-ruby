# FeaturesClient::FeaturesHasFeatureResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**has_feature** | **BOOLEAN** |  | [optional] 


