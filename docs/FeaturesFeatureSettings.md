# FeaturesClient::FeaturesFeatureSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**settings** | [**Hash&lt;String, FeaturesFeatureSetting&gt;**](FeaturesFeatureSetting.md) |  | [optional] 


