# FeaturesClient::FeaturesSyncEntityPackageRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**package** | **String** |  | [optional] 
**entities** | [**Array&lt;FeaturesEntity&gt;**](FeaturesEntity.md) |  | [optional] 
**changed_feature_names** | **Array&lt;String&gt;** |  | [optional] 


