# FeaturesClient::EntityFeaturesApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_feature_to_entity**](EntityFeaturesApi.md#add_feature_to_entity) | **POST** /v1/entity/features | 
[**delete_custom_entity_feature_settings**](EntityFeaturesApi.md#delete_custom_entity_feature_settings) | **DELETE** /v1/entity/{type}/{id}/features/{name}/settings | 
[**entity_has_feature**](EntityFeaturesApi.md#entity_has_feature) | **GET** /v1/entity/{type}/{id}/features/{name}/exists | 
[**entity_package_sync**](EntityFeaturesApi.md#entity_package_sync) | **POST** /v1/entity/features/sync_package | 
[**get_all_by_feature**](EntityFeaturesApi.md#get_all_by_feature) | **GET** /v1/features/{feature_name}/entity/{entity_type} | 
[**get_entity_feature**](EntityFeaturesApi.md#get_entity_feature) | **GET** /v1/entity/{type}/{id}/features/{name} | 
[**get_entity_feature_list**](EntityFeaturesApi.md#get_entity_feature_list) | **GET** /v1/entity/{type}/{id}/features | 
[**get_entity_feature_settings**](EntityFeaturesApi.md#get_entity_feature_settings) | **GET** /v1/entity/{type}/{id}/features/{name}/settings/{setting_type} | TODO - get array of features
[**get_entity_features**](EntityFeaturesApi.md#get_entity_features) | **GET** /v1/entity/{type}/{id}/all_features | 
[**is_entity_feature_enabled**](EntityFeaturesApi.md#is_entity_feature_enabled) | **GET** /v1/entity/{type}/{id}/features/{name}/enabled | 
[**set_entity_feature_settings**](EntityFeaturesApi.md#set_entity_feature_settings) | **POST** /v1/entity/{entity.type}/{entity.id}/features/{feature_name}/settings | 
[**update_entity_feature**](EntityFeaturesApi.md#update_entity_feature) | **PUT** /v1/entity/{entity.type}/{entity.id}/features/{feature_name} | 


# **add_feature_to_entity**
> FeaturesActionResponse add_feature_to_entity(body)



### Example
```ruby
# load the gem
require 'features-client'

api_instance = FeaturesClient::EntityFeaturesApi.new

body = FeaturesClient::FeaturesEntityFeature.new # FeaturesEntityFeature | 


begin
  result = api_instance.add_feature_to_entity(body)
  p result
rescue FeaturesClient::ApiError => e
  puts "Exception when calling EntityFeaturesApi->add_feature_to_entity: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**FeaturesEntityFeature**](FeaturesEntityFeature.md)|  | 

### Return type

[**FeaturesActionResponse**](FeaturesActionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **delete_custom_entity_feature_settings**
> FeaturesActionResponse delete_custom_entity_feature_settings(type, id, name)



### Example
```ruby
# load the gem
require 'features-client'

api_instance = FeaturesClient::EntityFeaturesApi.new

type = "type_example" # String | 

id = "id_example" # String | 

name = "name_example" # String | 


begin
  result = api_instance.delete_custom_entity_feature_settings(type, id, name)
  p result
rescue FeaturesClient::ApiError => e
  puts "Exception when calling EntityFeaturesApi->delete_custom_entity_feature_settings: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **String**|  | 
 **id** | **String**|  | 
 **name** | **String**|  | 

### Return type

[**FeaturesActionResponse**](FeaturesActionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **entity_has_feature**
> FeaturesHasFeatureResponse entity_has_feature(type, id, name, opts)



### Example
```ruby
# load the gem
require 'features-client'

api_instance = FeaturesClient::EntityFeaturesApi.new

type = "type_example" # String | 

id = "id_example" # String | 

name = "name_example" # String | 

opts = { 
  setting_type: "setting_type_example", # String | 
  settings: true # BOOLEAN | 
}

begin
  result = api_instance.entity_has_feature(type, id, name, opts)
  p result
rescue FeaturesClient::ApiError => e
  puts "Exception when calling EntityFeaturesApi->entity_has_feature: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **String**|  | 
 **id** | **String**|  | 
 **name** | **String**|  | 
 **setting_type** | **String**|  | [optional] 
 **settings** | **BOOLEAN**|  | [optional] 

### Return type

[**FeaturesHasFeatureResponse**](FeaturesHasFeatureResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **entity_package_sync**
> FeaturesActionResponse entity_package_sync(body)



### Example
```ruby
# load the gem
require 'features-client'

api_instance = FeaturesClient::EntityFeaturesApi.new

body = FeaturesClient::FeaturesSyncEntityPackageRequest.new # FeaturesSyncEntityPackageRequest | 


begin
  result = api_instance.entity_package_sync(body)
  p result
rescue FeaturesClient::ApiError => e
  puts "Exception when calling EntityFeaturesApi->entity_package_sync: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**FeaturesSyncEntityPackageRequest**](FeaturesSyncEntityPackageRequest.md)|  | 

### Return type

[**FeaturesActionResponse**](FeaturesActionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **get_all_by_feature**
> FeaturesEntityList get_all_by_feature(feature_name, entity_type)



### Example
```ruby
# load the gem
require 'features-client'

api_instance = FeaturesClient::EntityFeaturesApi.new

feature_name = "feature_name_example" # String | 

entity_type = "entity_type_example" # String | 


begin
  result = api_instance.get_all_by_feature(feature_name, entity_type)
  p result
rescue FeaturesClient::ApiError => e
  puts "Exception when calling EntityFeaturesApi->get_all_by_feature: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **feature_name** | **String**|  | 
 **entity_type** | **String**|  | 

### Return type

[**FeaturesEntityList**](FeaturesEntityList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **get_entity_feature**
> FeaturesEntityFeature get_entity_feature(type, id, name, opts)



### Example
```ruby
# load the gem
require 'features-client'

api_instance = FeaturesClient::EntityFeaturesApi.new

type = "type_example" # String | 

id = "id_example" # String | 

name = "name_example" # String | 

opts = { 
  setting_type: "setting_type_example", # String | 
  settings: true # BOOLEAN | 
}

begin
  result = api_instance.get_entity_feature(type, id, name, opts)
  p result
rescue FeaturesClient::ApiError => e
  puts "Exception when calling EntityFeaturesApi->get_entity_feature: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **String**|  | 
 **id** | **String**|  | 
 **name** | **String**|  | 
 **setting_type** | **String**|  | [optional] 
 **settings** | **BOOLEAN**|  | [optional] 

### Return type

[**FeaturesEntityFeature**](FeaturesEntityFeature.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **get_entity_feature_list**
> FeaturesFeaturesListResponse get_entity_feature_list(type, id, opts)



### Example
```ruby
# load the gem
require 'features-client'

api_instance = FeaturesClient::EntityFeaturesApi.new

type = "type_example" # String | 

id = "id_example" # String | 

opts = { 
  package_id: "package_id_example", # String | 
  package_name: "package_name_example", # String | 
  platform: "platform_example" # String | 
}

begin
  result = api_instance.get_entity_feature_list(type, id, opts)
  p result
rescue FeaturesClient::ApiError => e
  puts "Exception when calling EntityFeaturesApi->get_entity_feature_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **String**|  | 
 **id** | **String**|  | 
 **package_id** | **String**|  | [optional] 
 **package_name** | **String**|  | [optional] 
 **platform** | **String**|  | [optional] 

### Return type

[**FeaturesFeaturesListResponse**](FeaturesFeaturesListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **get_entity_feature_settings**
> FeaturesEntityFeatureSettings get_entity_feature_settings(type, id, name, setting_type, opts)

TODO - get array of features

### Example
```ruby
# load the gem
require 'features-client'

api_instance = FeaturesClient::EntityFeaturesApi.new

type = "type_example" # String | 

id = "id_example" # String | 

name = "name_example" # String | 

setting_type = "setting_type_example" # String | 

opts = { 
  settings: true # BOOLEAN | 
}

begin
  #TODO - get array of features
  result = api_instance.get_entity_feature_settings(type, id, name, setting_type, opts)
  p result
rescue FeaturesClient::ApiError => e
  puts "Exception when calling EntityFeaturesApi->get_entity_feature_settings: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **String**|  | 
 **id** | **String**|  | 
 **name** | **String**|  | 
 **setting_type** | **String**|  | 
 **settings** | **BOOLEAN**|  | [optional] 

### Return type

[**FeaturesEntityFeatureSettings**](FeaturesEntityFeatureSettings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **get_entity_features**
> FeaturesEntityFeaturesList get_entity_features(type, id, opts)



### Example
```ruby
# load the gem
require 'features-client'

api_instance = FeaturesClient::EntityFeaturesApi.new

type = "type_example" # String | 

id = "id_example" # String | 

opts = { 
  package_id: "package_id_example", # String | 
  package_name: "package_name_example", # String | 
  platform: "platform_example" # String | 
}

begin
  result = api_instance.get_entity_features(type, id, opts)
  p result
rescue FeaturesClient::ApiError => e
  puts "Exception when calling EntityFeaturesApi->get_entity_features: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **String**|  | 
 **id** | **String**|  | 
 **package_id** | **String**|  | [optional] 
 **package_name** | **String**|  | [optional] 
 **platform** | **String**|  | [optional] 

### Return type

[**FeaturesEntityFeaturesList**](FeaturesEntityFeaturesList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **is_entity_feature_enabled**
> FeaturesFeatureEnabledResponse is_entity_feature_enabled(type, id, name, opts)



### Example
```ruby
# load the gem
require 'features-client'

api_instance = FeaturesClient::EntityFeaturesApi.new

type = "type_example" # String | 

id = "id_example" # String | 

name = "name_example" # String | 

opts = { 
  setting_type: "setting_type_example", # String | 
  settings: true # BOOLEAN | 
}

begin
  result = api_instance.is_entity_feature_enabled(type, id, name, opts)
  p result
rescue FeaturesClient::ApiError => e
  puts "Exception when calling EntityFeaturesApi->is_entity_feature_enabled: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **String**|  | 
 **id** | **String**|  | 
 **name** | **String**|  | 
 **setting_type** | **String**|  | [optional] 
 **settings** | **BOOLEAN**|  | [optional] 

### Return type

[**FeaturesFeatureEnabledResponse**](FeaturesFeatureEnabledResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **set_entity_feature_settings**
> FeaturesEntityFeatureSettings set_entity_feature_settings(entity_type, entity_id, feature_name, body)



### Example
```ruby
# load the gem
require 'features-client'

api_instance = FeaturesClient::EntityFeaturesApi.new

entity_type = "entity_type_example" # String | 

entity_id = "entity_id_example" # String | 

feature_name = "feature_name_example" # String | 

body = FeaturesClient::FeaturesSetEntityFeatureSettingsRequest.new # FeaturesSetEntityFeatureSettingsRequest | 


begin
  result = api_instance.set_entity_feature_settings(entity_type, entity_id, feature_name, body)
  p result
rescue FeaturesClient::ApiError => e
  puts "Exception when calling EntityFeaturesApi->set_entity_feature_settings: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entity_type** | **String**|  | 
 **entity_id** | **String**|  | 
 **feature_name** | **String**|  | 
 **body** | [**FeaturesSetEntityFeatureSettingsRequest**](FeaturesSetEntityFeatureSettingsRequest.md)|  | 

### Return type

[**FeaturesEntityFeatureSettings**](FeaturesEntityFeatureSettings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **update_entity_feature**
> FeaturesActionResponse update_entity_feature(entity_type, entity_id, feature_name, body)



### Example
```ruby
# load the gem
require 'features-client'

api_instance = FeaturesClient::EntityFeaturesApi.new

entity_type = "entity_type_example" # String | 

entity_id = "entity_id_example" # String | 

feature_name = "feature_name_example" # String | 

body = FeaturesClient::FeaturesUpdateEntityFeatureRequest.new # FeaturesUpdateEntityFeatureRequest | 


begin
  result = api_instance.update_entity_feature(entity_type, entity_id, feature_name, body)
  p result
rescue FeaturesClient::ApiError => e
  puts "Exception when calling EntityFeaturesApi->update_entity_feature: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entity_type** | **String**|  | 
 **entity_id** | **String**|  | 
 **feature_name** | **String**|  | 
 **body** | [**FeaturesUpdateEntityFeatureRequest**](FeaturesUpdateEntityFeatureRequest.md)|  | 

### Return type

[**FeaturesActionResponse**](FeaturesActionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



