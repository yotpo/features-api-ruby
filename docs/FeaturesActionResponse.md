# FeaturesClient::FeaturesActionResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** |  | [optional] 
**changed** | **BOOLEAN** |  | [optional] 


