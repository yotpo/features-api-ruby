# FeaturesClient::FeaturesUpdateEntityFeatureRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entity** | [**FeaturesEntity**](FeaturesEntity.md) |  | [optional] 
**feature_name** | **String** |  | [optional] 
**user_enabled** | [**FeaturesNilOrBool**](FeaturesNilOrBool.md) |  | [optional] 
**skip_dependencies** | **BOOLEAN** |  | [optional] 
**disable** | **BOOLEAN** |  | [optional] 
**set_user_state** | **BOOLEAN** |  | [optional] 


