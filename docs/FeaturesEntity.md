# FeaturesClient::FeaturesEntity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | [optional] 
**id** | **String** |  | [optional] 
**package** | [**FeaturesPackage**](FeaturesPackage.md) |  | [optional] 
**platform** | **String** |  | [optional] 


