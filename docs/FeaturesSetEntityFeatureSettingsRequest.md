# FeaturesClient::FeaturesSetEntityFeatureSettingsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entity** | [**FeaturesEntity**](FeaturesEntity.md) |  | [optional] 
**feature_name** | **String** |  | [optional] 
**settings** | **Hash&lt;String, String&gt;** |  | [optional] 


