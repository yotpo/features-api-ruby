# FeaturesClient::FeaturesSimpleHash

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**simple_hash** | **Hash&lt;String, String&gt;** |  | [optional] 


