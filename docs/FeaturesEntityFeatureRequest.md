# FeaturesClient::FeaturesEntityFeatureRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | [optional] 
**id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**setting_type** | **String** |  | [optional] 
**settings** | **BOOLEAN** |  | [optional] 


