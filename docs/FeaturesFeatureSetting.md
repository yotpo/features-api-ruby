# FeaturesClient::FeaturesFeatureSetting

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_level** | **String** |  | [optional] 
**private** | **BOOLEAN** |  | [optional] 
**default_value** | **String** |  | [optional] 


