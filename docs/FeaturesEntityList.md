# FeaturesClient::FeaturesEntityList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entities** | [**Array&lt;FeaturesEntity&gt;**](FeaturesEntity.md) |  | [optional] 


